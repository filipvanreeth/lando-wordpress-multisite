# Setup

Make sure Lando is installed on your (local) environment. If not you can download and install Lando [here](https://docs.lando.dev/basics/installation.html).

For quick development and debugging we added the WordPress core files into the repository. So after following the steps below the WordPress instance should be running in a few seconds.

## Steps
- Setup the domain `lando-wordpress-multisite.local` in your hosts file.
- Run `lando start` to setup the WordPress instance.
- Run `lando composer install` to install all dependencies.
- Run `lando wp db import ./db/lando-wordpress-multisite.sql --path=wordpress` to import the database.


Great, you have a running WordPress Multisite instance with two subsites:
- https://lando-wordpress-multisite.local/site-1/
- https://lando-wordpress-multisite.local/site-2/

You can login via https://lando-wordpress-multisite.local/wp-admin/ with the following credentials:
  - Username: `admin`
  - Password: `admin`

## Todo
This WordPress setup uses Multisite with subdirectories instead of subdomains. This means that there need to be specific NGINX configuration rules to be setup. You can find all information about this via https://docs.lando.dev/config/nginx.html#supported-versions.

Your task is to setup the correct `server`, `vhosts` and `params` settings for NGINX via Lando.
